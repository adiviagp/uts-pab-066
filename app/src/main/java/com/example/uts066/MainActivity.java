package com.example.uts066;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void clickHitam(View view)
    {
        Intent intent = new Intent(this, OrderActivity.class);
        String hitam = "Anggur Hitam";
        String hargaHitam = "30000";

        intent.putExtra("EXTRA_HITAM", hitam);
        intent.putExtra("EXTRA_HITAM_PRICE", hargaHitam);

        startActivity(intent);
    }
    public void clickMerah(View view)
    {
        Intent intent = new Intent(this, OrderActivity.class);
        String merah = "Anggur Merah";
        String hargaMerah= "40000";

        intent.putExtra("EXTRA_MERAH", merah);
        intent.putExtra("EXTRA_MERAH_PRICE", hargaMerah);

        startActivity(intent);
    }
    public void clickApel(View view)
    {
        Intent intent = new Intent(this, OrderActivity.class);
        String apel = "Apel";
        String hargaApel= "35000";

        intent.putExtra("EXTRA_APEL", apel);
        intent.putExtra("EXTRA_APEL_PRICE", hargaApel);

        startActivity(intent);
    }
    public void clickMangga(View view)
    {
        Intent intent = new Intent(this, OrderActivity.class);
        String mangga = "Mangga";
        String hargaMangga= "20000";

        intent.putExtra("EXTRA_MANGGA", mangga);
        intent.putExtra("EXTRA_MANGGA_PRICE", hargaMangga);

        startActivity(intent);
    }
//
public void clickHitam2(View view)
{
    String url = "https://lifestyle.sindonews.com/read/1278773/155/10-manfaat-anggur-hitam-mulai-fungsi-otak-hingga-kulit-1517539621";
    Intent i = new Intent(Intent.ACTION_VIEW);
    i.setData(Uri.parse(url));
    startActivity(i);
}
    public void clickMerah2(View view)
    {
        String url = "https://www.idntimes.com/health/fitness/susi-yanti-nurraini/manfaat-anggur-merahampuh-mengatasi-wajah-berminyak-exp-c1c2";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }
    public void clickApel2(View view)
    {
        String url = "https://blog.regopantes.com/inspirasi/apel-fuji/";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }
    public void clickMangga2(View view)
    {
        String url = "https://hellosehat.com/hidup-sehat/tips-sehat/manfaat-makan-buah-mangga/";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }
}
