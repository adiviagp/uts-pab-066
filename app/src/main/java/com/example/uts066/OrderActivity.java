package com.example.uts066;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

public class OrderActivity extends AppCompatActivity {

    EditText nama, alamat, telp, note;
    Spinner jumlah;
    RadioGroup bayar;
    TextView detailBuah, detailPesanan;
    String buah, harga, metode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        Spinner spinner = (Spinner) findViewById(R.id.jumlah_pembelian);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.jumlah_pembelian, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        nama = findViewById(R.id.nama);
        alamat = findViewById(R.id.alamat);
        telp = findViewById(R.id.telp);
        note = findViewById(R.id.note);
        jumlah = findViewById(R.id.jumlah_pembelian);
        bayar = findViewById(R.id.bayar);
        detailBuah = findViewById(R.id.detailBuah);
        detailPesanan = findViewById(R.id.detailPesanan);

        Intent intent = getIntent();

        if(intent.hasExtra("EXTRA_HITAM")) {
            buah = intent.getStringExtra("EXTRA_HITAM");
            harga = intent.getStringExtra("EXTRA_HITAM_PRICE");
        }
        if(intent.hasExtra("EXTRA_MERAH")) {
            buah = intent.getStringExtra("EXTRA_MERAH");
            harga = intent.getStringExtra("EXTRA_MERAH_PRICE");
        }
        if(intent.hasExtra("EXTRA_APEL")) {
            buah = intent.getStringExtra("EXTRA_APEL");
            harga = intent.getStringExtra("EXTRA_APEL_PRICE");
        }
        if(intent.hasExtra("EXTRA_MANGGA")) {
            buah = intent.getStringExtra("EXTRA_MANGGA");
            harga = intent.getStringExtra("EXTRA_MANGGA_PRICE");
        }

        detailBuah.setText(buah + " harga per kg : Rp. " + harga + ", Minimal 1 Kg, maksimal 4Kg");

    }
    public void submitPesanan(View view)
    {
        String GetNama = nama.getText().toString();
        String GetAlamat = alamat.getText().toString();
        String GetTelp = telp.getText().toString();
        String GetNote = note.getText().toString();
        String GetJumlah = jumlah.getSelectedItem().toString();
        int GetBayar = bayar.getCheckedRadioButtonId();
        if (GetBayar != -1) {
            RadioButton selectedRadioButton = (RadioButton) findViewById(GetBayar);
            metode = selectedRadioButton.getText().toString();
        }

        int jumlahh = Integer.parseInt(jumlah.getSelectedItem().toString());;
        int hargaa = Integer.parseInt(harga);
        int total = jumlahh * hargaa;

        detailPesanan.setText("Pesanan untuk " + GetNama + " sedang diproses -- detail pemesanan -- pilihan buah : " + buah + " total harga : " + total + " metode pembayaran :" + metode);
    }
}
